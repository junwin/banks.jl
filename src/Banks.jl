module Banks
using Dates
using TimeZones
#using FixedPointDecimals

#greet() = print("Hello World!")

whichbank() = "Banks.jl/src/Banks.jl"

struct Balance
    amount::Real
    timestamp::ZonedDateTime
end

# Convert old DateTime constructor to ZonedDateTime constructor
Balance(amount::Real, timestamp::DateTime) = Balance(amount, ZonedDateTime(timestamp, localzone()))
#Balance{A}(amount::A, timestamp::DateTime) where {A <: Real} = Balance(amount, ZonedDateTime(timestamp, localzone()))

# Empty constructor
Balance() = Balance(zero(0.), now(localzone()))
#Balance{A}() where {A <: Real} = Balance(zero(A), now(localzone()))

# Default timestamp to now in local timezone
Balance(amount::Real) = Balance(amount, now(localzone()))

# No specific Real type specified - default to Float
Base.zero(::Type{Balance}) = zero(Balance(0.))

# Specify a specific Real sub-type????
#Base.zero(::Type{Balance{A}}) where {A <: Real} = Balance(zero(A), ZonedDateTime(0, tz"UTC"))

# Zero timestamp is zero in UTC
Base.zero(bal::Balance) = Balance(zero(bal.amount), ZonedDateTime(0, tz"UTC"))
#Base.zero(B::Banks.Balance) = Banks.Balance(zero(B.amount), DateTime(0, 1, 1))

Base.:+(b1::Balance, b2::Balance) = Balance(b1.amount + b2.amount, max(b1.timestamp, b2.timestamp))

struct Transaction
    amount::Real
    timestamp::ZonedDateTime
    description::Union{String, Nothing}
#    description::String
end

# Default `timestamp` and `description`
function Transaction(amount::Real)
    Transaction(amount, now(), nothing)
end

# Default `description`
function Transaction(amount::Real, timestamp::ZonedDateTime)
    Transaction(amount, timestamp, nothing)
end

function Transaction(amount::Real, datetime::DateTime)
    Transaction(amount, datetime, localzone())
end

function Transaction(amount::Real, datetime::DateTime, timezone::TimeZone)
    Transaction(amount, ZonedDateTime(datetime, timezone), nothing)
end

# Default `timestamp` - current time in the local timezone
function Transaction(amount::Real, description::Union{String, Nothing})
    Transaction(amount, now(localzone()), description)
end

# Default `timestamp` - current time in the local timezone
function Transaction(amount::Real, datetime::DateTime, description::Union{String, Nothing})
    Transaction(amount, ZonedDateTime(datetime, localzone()), description)
end

# Default `timestamp` - current time in the local timezone
function Transaction(amount::Real, datetime::DateTime, timezone::TimeZone, description::Union{String, Nothing})
    Transaction(amount, ZonedDateTime(datetime, timezone), description)
end

Base.:+(t1::Transaction, t2::Transaction) = Transaction(t1.amount + t2.amount, max(t1.timestamp, t2.timestamp))

struct Account
    name::String
    balance::Balance #{N <: Real}
#    transactions::Array{Transaction}
end

mutable struct TransactionAccount
    name::String
    balance::Balance
    transactions::Array

    # Insist that the initial balance matches the transactions
    TransactionAccount(name, balance, transactions) = 
        !isapprox(sum(transactions).amount, balance.amount, atol=0.001) ? 
            error("balance is not consistent with total of transactions: $(balance.amount) vs. $(sum(transactions).amount)") : 
            new(name, balance, transactions)

end

"""
function TransactionAccount(name::String, balance::Balance, transactions::Array{Transaction})
    balance = zero(Banks.Balance)
    TransactionAccount(name, balance, Array{Transaction}(undef,0))
end
"""

function TransactionAccount(name::String)
    TransactionAccount(name, zero(Balance))
end

# Allow construnction without specifying numeric type by providing a default type of `Float64`
#TransactionAccount(name::String) = TransactionAccount{Float64}(name::String)

function TransactionAccount(name::String, balance::Balance)
    # Create a Transaction reflecting the initial Balance
    TransactionAccount(name, balance, [Transaction(balance.amount, balance.timestamp, "Initial transaction")])
end

#function TransactionAccount(name::String, balance::Balance)
#    TransactionAccount(name, balance)
#end

function post!(ta::TransactionAccount, tx::Transaction)
    # Turn debugging on with `ENV["JULIA_DEBUG"] = Banks`
    @debug "ta.transactions $ta.transactions"
    @debug "tx: $tx"
#    ta.transactions = vcat(ta.transactions, tx)

    # Should duplicate transactions be prevented? They could be legitimate!
    #=
    for t in ta.transactions
        @debug "equal?:  $(t == tx)"
        if isequal(t, tx)
            throw(ArgumentError("the Transaction aready exists in the TransactionAccount transactions"))
        end
    end
    =#

    push!(ta.transactions, tx)
    #println(size(ta.transactions))
    #println(ta.transactions)
    #ta.balance.amount += tx.amount

    # newbalance = Balance(round(ta.balance.amount + tx.amount, digits=2), now())
    newbalance = Balance(ta.balance.amount + tx.amount, now())
    ta.balance = newbalance

    #@assert round(sum(t.amount for t in ta.transactions), digits=2) == ta.balance.amount
    @assert totalamount(ta.transactions) == ta.balance.amount "$(totalamount(ta.transactions)) != $(ta.balance.amount)"
end

#totalamount(txns::Array{Transaction}) = round(sum(t.amount for t in txns), digits=2)
totalamount(txns::Array{Transaction}) = sum(t.amount for t in txns)

function totalamount(ta::TransactionAccount) # {N<:Real}
    if length(ta.transactions) > 0
       totalamount(ta.transactions)
    else
       zero(ta.balance.amount)
    end
end

end # module
