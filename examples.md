# Bank.jl module

This document describes the operation of the Bank.

## The bank

Is this (the Bank) needed? Or assume the existence of it?

Reasons _for_ having this construct:

- Can be the repository of information needed generally by other constructs. E.g.:
  - Default time zone
  - Default currency
  - The available features (not every bank might want to offer all features)

Not implemented.

```jl
bank = Bank("The Family Bank")
```

The bank could be associated with an Account. E.g.:

```jl
bank = Bank("The Family Bank")   # Is this needed? Or assume the existence of it?
bta = Bank.TransactionAccount("Sams's new account", bank=bank)`
```

But it should be optional, for simple use cases being able to create and manipulate a simple account (without a lot of set-up) is desirable.

See [Nothingness and missing values](https://docs.julialang.org/en/v1/manual/faq/#Nothingness-and-missing-values).

> For situations where a value x of type T exists only sometimes, the Union{T, Nothing} type can be used for function arguments, object fields and array element types as the equivalent of Nullable, Option or Maybe in other languages.

## Person Name

Not implemented.

## Person

Not implemented.

```jl
p1 = Person(name::PersonName)
```

## Entity

Not implemented.

An entity might be a generic person or organisation.

An entity could be a bank, or a person.

What about a joint account? Would the owner also be an entity?
See next section where joint customers are discussed.

## Bank customers

Not implemented.

The joint account is definitely not implemented as yet.

```jl
bcs = Bank.BankCustomer("Bloggs", "Samuel")   # id=uuid, 
bcj = Bank.BankCustomer("Smith", "James")
bcjoint = Bank.BankCustomer(bc1, bc2)     # Assumes that a customer can be 
```

## Bank account

Implemented as `Account`. Does not yet have an association with `BankCustomer`.

Initial constructor takes a `Balance` argument.

```jl
bas = Bank.Account("Sams's new account", Bank.Balance(0., DateTime(now())))`
```

### To do

- [x] There should probably be a different constructor without the `Balance` that
automatically creates a zero `Balance`.

```jl
bas = Bank.Account("Sams's new account")`
```

```jl
baj = Bank.BankAccount("Sams's new account", bcs)
bas = Bank.BankAccount("Jim's account", bcj)
bajoint = Bank.BankAccount("Sam and Jim's joint account", bcjoint)
```

## TransactionAccount

The current `Account` doesn't have `Transaction`s (so what is the point of it?).

Instead a different `TransactionAccount` has an array of `Transaction`.

### balance()

```jl
function balance(::TransactionAccount)::Balance
```

```jl
# Return the balance at (or most recently prior) the `at` datetime.
function balance(::TransactionAccount, at::DateTime)::Balance

# or...
function balance(::TransactionAccount, at::DateTime)::Union{Balance, Nothing}
```

Questions:

- What if there is no sensible balace for the `at` value? E.g. it is prior to the first transaction.
Perhaps the value returned ought to be `Union{Nothing, Balance}`?
Or a generated zero balance dated at the first occurance of the TransactionAccount?
- How should `balance(::TransactionAccount)` be used versus `ta.balance`?

### TransactionAccount To do

- [ ] Identify the account with an account number
- [ ] Link the account to an owner (`BankCustomer` or other `Entity`?)
- [ ] Link the account to a `Bank` to, for example, get available `AccountFeature`s
- [ ] Capture the creation datetime of the TransactionAccount. Useful for generating a Balance where not relevant transactions exist.

```jl
# Link the account to a `Entity`
function TransactionAccount(name::String, balance::Balance, transactions::Array{Transaction}, owner::Union{Entity, Nothing})
   ...
end
```

```jl
# Link the account to a `Bank`
function TransactionAccount(name::String, balance::Balance, transactions::Array{Transaction}, bank::Union{Bank, Nothing})
   ...
end
```

## Transaction

```jl
tplus = Bank.Transaction(12.34, DateTime(now()))
tneg = Bank.Transaction(-234.56, DateTime(now()))
```

With a `description`:

```jl
tplus = Bank.Transaction(12.34, DateTime(now()), "A transaction")
tneg = Bank.Transaction(-234.56, DateTime(now()), "A transaction")
```

The `description` part was not initially implemented. It was added later.

## post! (a `Transaction` to an `Account`)

```jl
post!(ta::Bank.TransactionAccount, t::Bank.Transaction) 
```

## Balance

Holds a specific account balance and is updated (or more likely replaced with a new balance) whenever transactions are processed on a Transaction Account.

The purpose is to avoid needing to access and process historical transactions to get the current (or any historical) position.

Possible options:

- Calculate a new Balance for every transaction
- Have a Balance reference the prior Balance to enable chaining back over all Balances
- Explicitly link the Balance to the Transaction that triggered its creation (link may go either way or both ways)
- Hold a full list of past Balances - probably held in time order

Balances other than the current balance can be useful.

A Balance can be one (or more than one) of:

- Current balance - i.e. the balance at the present time
- End of day balance - significant because other calculations (e.g. feature calculations) are likely to be based on end of day balances
- End of month balance - significant because other calculations (e.g. feature calculations like payment of interest accruals) are likely to be based on end of day balances
- Historical balance - what used to be a current balance but has been superceded
- Future balance - a prediction of a future balance based on currently known information

## Account Features (e.g. Interest Calculation)

Accounts need a mechanism for customising characteristics. E.g.

- Interest calculations
- Interest rates
- Fees
- Automated features such as automatic transfers (e.g. when the balance is greater than $100 move $10 to a savings account)

Account features may be inherited from a Bank.
E.g. to allow consistency across all similar accounts for a Bank and to allow different banks to operate differently.

Account features should be "consulted" whenever transactions are processed.
Each feature may then initiate processing if the feature triggers match the circumstances.

```jl
function isfeatureinterested(feature, transaction)::Bool
   # Assumes that everything needed (account, etc.) is available via the `transaction`
   # return true or false
end
```

Perhaps better to directly reference the `TransactionAccount`:

```jl
function isfeatureinterested(feature::Feature, transactionaccount::TransactionAccount, transaction::Transaction)::Bool
   # return true or false
end
```

```jl
function consultfeature(feature, transaction)::
   # return true or false
end
```

Some features may be triggered by external events.
E.g. A calendar event such as the end of day or end of month may initiate feature execution.

E.g. "Interest accrual" triggered at "End of month".

A feature may be triggered by:

- A schedule. E.g. end of businesws day, end of calendar month
- An event. E.g. every transaction, deposit transaction, withdrawal transaction, balance situation (e.g. less than zero), propective balance situation (balance will be less than zero if the transaction proceeds).

Questions:

- Ordering or priority? Do features need to be consulted in a particular order?
- Recursion! What if executing a feature triggers a different feature that triggers the first (etc., etc.)?

## Timestamps

What about international transactions? There needs to be a reliable way to combine transactions from all time zones so that they are ordered or sequenced correctly.
A transaction occuring at 10am local Syndey time cannot be processed (or be regarded as preceding) a transaction occuring at 11am NZ time.

Transfers between time zones must be able to reflect that theu occur simulataneoulsy in each time zone even if the local times differ.

E.g. a sequence of transactions occuring in different time zones need to be sequenced in a canonical form such as UTC.

 ID | Local time | UTC |
| --- | --- | --- |
| T1 | 11:00am 2021-02-02 NZDT (UTC +13) | 10:00pm 2012-02-01 UTC |
| T2 | 09:05am 2021-02-02 AEDT (UTC +11) | 10:05pm 2012-02-01 UTC |
| T3 | 11:06am 2021-02-02 NZDT (UTC +13) | 10:06pm 2012-02-01 UTC |
| T4 (e.g. transfer to NZ) | 10:07pm 2021-02-01 GMT (UTC +0)   | 10:07pm 2012-02-01 UTC |
| T5 (contra to T4, simultaneous) | 11:07am 2021-02-01 NZDT (UTC +13) | 10:07pm 2012-02-01 UTC |

- Tag every transaction with a UTC timestamp?
- Associate every account with a "home" time zone?
- Give every bank have a default time zone? Inherited by default by the accounts in that bank.
- Convert timestamps to UTC on the fly?

Some calendar event processing will need to occur according to the local time of the host account.

E.g. if interest accrual (calendar event triggered) occurs at midnight, then it will occur at different times for accounts hosted in different time zones. E.g. NZ ans Aus on summer time, UK on standard time - GMT):

| ID | Local time | UTC |
| --- | --- | --- |
| T1 | 11:00am 2021-02-02 NZDT (UTC +13) | 10:00pm 2012-02-01 UTC |
| T2 | 09:05am 2021-02-02 AEDT (UTC +11) | 10:05pm 2012-02-01 UTC |
| T3 | 11:06am 2021-02-02 NZDT (UTC +13) | 10:06pm 2012-02-01 UTC |
| T4 (e.g. transfer to NZ) | 10:07pm 2021-02-01 GMT (UTC +0)   | 10:07pm 2012-02-01 UTC |
| T5 (contra to T4, simultaneous) | 11:07am 2021-02-01 NZDT (UTC +13) | 10:07pm 2012-02-01 UTC |

Or at a different time of year (i.e. UK on summer time, NZ and Aus on their respective standard times):

| Account ID | "Home" time zone | Local time | UTC |
| ---| --- | --- | --- |
| ACNZ1 | NZDT | 12:00am midnight 2021-02-02 NZDT (UTC +13) | 11:00 2012-02-01 UTC |
| ACNZ2 | NZDT | 12:00am midnight 2021-02-02 NZDT (UTC +13) | 11:00 2012-02-01 UTC |
| ACAU1 | AEDT | 12:00am midnight 2021-02-02 AEDT (UTC +11) | 13:00 2012-02-01 UTC |
| ACUK1 | BST  | 12:00am midnight 2021-02-02 GMT (UTC +0)   | 12:00 midnight 2021-02-02 UTC |

## General To Do

- [ ] See if `accumulate(op, A; dims::Integer, [init])` can replace `sum(t.amount for t in ta.transactions)`. See [Base.accumulate](https://docs.julialang.org/en/v1/base/arrays/#Base.accumulate)
- [ ] Import csv file of transactions. Use CSV.jl

## Done

Giving the module the same name is probably an issue. Perhaps need to rename the module to `Banks.jl`.

- [X] Create + functions for Balance, Transaction. E.g.

```jl
Base.:+(t1::Transaction, t2::Transaction) = Transaction(t1.amount + t2.amount, max(t1.datetime, t2.datetime))
```

- [X] Rename `transactiontime` and `balancetime` to `timestamp`
