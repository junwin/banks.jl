import Banks
using Test
using Dates
using TimeZones
using Random
using FixedPointDecimals

#Banks.greet()
#Banks.whichbank()

# Uncomment one of the following to activate debug logging:
#ENV["JULIA_DEBUG"] = Banks
#ENV["JULIA_DEBUG"] = all

# rounding tolerance
tolerance = 0.00001

@testset "Banks tests" begin
    # An exception (`ArgumentError`) will be thrown if the timezones have not been built.
    # See details under `@testset "+(Balance) tests"`
    @test Banks.whichbank() == "Banks.jl/src/Banks.jl"
end

@testset "Ensure timezones are available" begin
    @test tz"Pacific/Auckland" == TimeZone("Pacific/Auckland")
end

@testset "Balance constructor tests" begin
    dt = DateTime(2020, 12, 7, 20, 05)
    zdt = ZonedDateTime(2020, 12, 7, 20, 05, localzone())

    # Default Float64
    @test Banks.Balance(12.34, DateTime(now())).amount == 12.34
    @test Banks.Balance(12.34, dt).timestamp == zdt
    @test Banks.Balance(12.34, zdt).timestamp == zdt
    @test typeof(Banks.Balance(12.34, DateTime(now())).amount) == Float64

    # Explicit Float64
    @test Banks.Balance(Float64(12.34), DateTime(now())).amount == 12.34
    @test Banks.Balance(Float64(12.34), dt).timestamp == zdt
    @test Banks.Balance(Float64(12.34), zdt).timestamp == zdt
    @test typeof(Banks.Balance(Float64(12.34), DateTime(now())).amount) == Float64

    # Explicit Float32
    @test Banks.Balance(Float32(12.34f0), DateTime(now())).amount == 12.34f0
    @test Banks.Balance(Float32(12.34f0), dt).timestamp == zdt
    @test Banks.Balance(Float32(12.34f0), zdt).timestamp == zdt
    @test typeof(Banks.Balance(Float32(12.34f0), DateTime(now())).amount) == Float32

    # Explicit Float32 by value
    @test Banks.Balance(12.34f0, DateTime(now())).amount == 12.34f0
    @test Banks.Balance(12.34f0, dt).timestamp == zdt
    @test Banks.Balance(12.34f0, zdt).timestamp == zdt
    @test typeof(Banks.Balance(12.34f0, DateTime(now())).amount) == Float32

    # Int32
    @test Banks.Balance(Int32(12), DateTime(now())).amount == 12
    @test Banks.Balance(Int32(12), dt).timestamp == zdt
    @test Banks.Balance(Int32(12), zdt).timestamp == zdt
    @test typeof(Banks.Balance(Int32(12), DateTime(now())).amount) == Int32

    # Int64 by value
    @test Banks.Balance(12, DateTime(now())).amount == 12
    @test Banks.Balance(12, dt).timestamp == zdt
    @test Banks.Balance(12, zdt).timestamp == zdt
    @test typeof(Banks.Balance(12, DateTime(now())).amount) == Int64

    # FixedPointDecimals
    @test Banks.Balance(FixedDecimal{Int, 2}(23.45), dt).amount == FixedDecimal{Int, 2}(23.45)
    @test Banks.Balance(FixedDecimal{Int, 2}(23.45), dt).timestamp == zdt
    @test Banks.Balance(FixedDecimal{Int, 2}(23.45), zdt).timestamp == zdt
    @test typeof(Banks.Balance(FixedDecimal{Int, 2}(23.45), zdt).amount) == FixedDecimal{Int, 2}

    # Partial constructor
    beforetime = now(localzone())
    bal = Banks.Balance(12.34)
    @test bal.amount == 12.34
    @test bal.timestamp >= beforetime && bal.timestamp <= now(localzone())

    # Specify different number type
    @test Banks.Balance(FixedDecimal{Int, 2}(0.)).amount == 0.
    @test Banks.Balance(zero(FixedDecimal{Int, 2})).amount == 0.
    @test typeof(Banks.Balance(zero(FixedDecimal{Int, 2})).amount) == FixedDecimal{Int, 2}

    # Empty constructor
    beforetime = now(localzone())
    bal = Banks.Balance()
    @test bal.amount == 0.
    @test bal.timestamp >= beforetime && bal.timestamp <= now(localzone())
end

@testset "zero(Balance) tests" begin
    # zero()
    nowtime = DateTime(now())
    zonednowtime = ZonedDateTime(nowtime, localzone())
    zerotime = DateTime(0)
    zonedzerotime = ZonedDateTime(DateTime(0), tz"UTC")

    # Float64
    @test zero(Banks.Balance(12.34, nowtime)).amount == 0.
    @test zero(Banks.Balance(12.34, nowtime)).timestamp == zonedzerotime
    # Int64
    @test zero(Banks.Balance(12, nowtime)).amount == 0
    @test zero(Banks.Balance(12, nowtime)).timestamp == zonedzerotime

    # Test the use in creating TransactionAccount - empty constructor
    # Float64
    @test zero(Banks.Balance) == zero(Banks.Balance(12.34, nowtime))
    @test zero(Banks.Balance(Float64(0.))) == zero(Banks.Balance(12.34, nowtime))
    @test typeof(Banks.Balance(Float64(0.)).amount) == Float64

    # FixedPointDecimals
    @test zero(Banks.Balance(FixedDecimal{Int, 2}(0))) == zero(Banks.Balance(FixedDecimal{Int, 2}(1), nowtime))
    @test typeof(zero(Banks.Balance(FixedDecimal{Int, 2}(0))).amount) == FixedDecimal{Int, 2}

    # Int
    @test zero(Banks.Balance(Int(0))) == zero(Banks.Balance(1, nowtime))
    @test typeof(zero(Banks.Balance(FixedDecimal{Int, 2}(123))).amount) == FixedDecimal{Int, 2}

    # Default Real type - amount will be the platform default Float
    @test zero(Banks.Balance) == zero(Banks.Balance(12.34, zonednowtime))
    @test zero(Banks.Balance).amount == 0.
    @test zero(Banks.Balance).timestamp == zonedzerotime
end;

@testset "+(Balance) tests" begin
    # zero()
    nowtime = DateTime(now())
    zerotime = DateTime(0)

    #=
    Note that the `Balance`s that are generated differ in the timezones of the timestamps
    if the full timezone data is not available.
    Instead of a timestamp being generated with, for example, running in a time zone of
    "Pacific/Auckland", it gets generated with something like `local (UTC+12/UTC+13)` ... i.e.
    the correct offset from UTC but not explicitly "Pacific/Auckland".
    That can mean that even though the Balances are nominally the same, even with equivalent timestanmps, 
    they fail the equality test.
    
    Trying to be explict with the time zone fails. E.g.
    `time3 = ZonedDateTime(DateTime(2021, 04, 28, 21, 15), tz"Pacific/Auckland")`
    ERROR: ArgumentError: Unable to find time zone "Pacific/Auckland". Try running `TimeZones.build()`.`

    Running `TimeZones.build()` command seems to use the explicit time zone of (e.g.) "Pacific/Auckland"
    from `localzone()`.

    E.g. Running `TimeZones.build()`:
    julia> TimeZones.build()
    [ Info: Installing 2020d tzdata region data
    [ Info: Converting tz source files into TimeZone data
    [ Info: Successfully built TimeZones

    And the following tests then pass.
    =#
    @test Banks.Balance(12.34, nowtime) + Banks.Balance(12.34, nowtime) == Banks.Balance(12.34*2, nowtime)
    @test Banks.Balance(FixedDecimal{Int, 2}(23.45), DateTime(2020, 12, 7, 20, 05)) + 
        Banks.Balance(FixedDecimal{Int, 2}(123.45), DateTime(2020, 12, 8, 20, 05)) ==
        Banks.Balance(FixedDecimal{Int, 2}(23.45 + 123.45), DateTime(2020, 12, 8, 20, 05))
end;

@testset "Account tests" begin
    b = Banks.Balance(12.34, DateTime(DateTime(2020, 12, 7, 20, 05)))
    @test Banks.Account("Test", b).balance == b
    @test Banks.Account("Test", b).name == "Test"
end

#starttime = DateTime(2020, 12, 7, 20, 07)

#timesgen = [DateTime(2020, 12, d, h, m) for d in 7:11, h in 9:13, m in 12:16]
times = reshape([ZonedDateTime(DateTime(2020, 12, d, h, m), localzone()) for d in 7:11, h in 9:13, m in 12:16], 125, 1)
#println("Times:", times)
#println(times[2])
#println("size(times)", size(times))

#amounts = -12.34:1.23:165.
amounts = rand(MersenneTwister(1), -12345.45:23456.56, 125)
#amounts = Vector{FixedDecimal{2}}
#println("Amounts:", amounts)
#println("size(Amounts):", size(amounts))

randomdescription(seed) = randstring(MersenneTwister(seed), "          abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 20)
descriptions = [randstring(MersenneTwister(seed), "          abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 20) for seed in 1:125]

@debug "size(Amounts): $(size(amounts))" 

#println("length(Amounts):", length(amounts))
#println("Amount[1]:", amounts[1, 1])


time1 = times[1]
t1 = Banks.Transaction(12.45, time1, "Transaction Description")
#t2 = Banks.Transaction(12.45, time1, Nothing)
t2 = Banks.Transaction(12.45, time1)
#txns = [Banks.Transaction(a, times[i], randomdescription(i)) for a in amounts, i in 1:5]
txns = [Banks.Transaction(amounts[i], times[i], descriptions[i]) for i in 1:125]

txnssum = sum(txns)                     # a Transaction
total = round(txnssum.amount, digits=3) # a number

#println(sum(t.amount for t in txns))
@debug "sum(t.amount for t in txns): $(sum(t.amount for t in txns))"

# Matching transactions with opposite amounts
txnsneg = map(x -> Banks.Transaction(x.amount * -1, x.timestamp, x.description), txns)
@debug "sum(t.amount for t in txnsneg): $(sum(t.amount for t in txnsneg))"
@debug "sum(t.amount for t in txns) + sum(t.amount for t in txnsneg): $(sum(t.amount for t in txns) + sum(t.amount for t in txnsneg))"

#println(txnsneg)

#println(txns)

@testset "Transaction tests (amount, ZonedDateTime)" begin
    @test Banks.Transaction(12.45, time1).amount == 12.45
    # ZonedDateTime, localzone
    @test Banks.Transaction(12.45, ZonedDateTime(DateTime(2020, 12, 7, 20, 07), localzone())).timestamp == ZonedDateTime(DateTime(2020, 12, 7, 20, 07), localzone())
    # ZonedDateTime, specified timezone
    @test Banks.Transaction(12.45, ZonedDateTime(DateTime(2020, 12, 7, 20, 07), tz"Australia/Sydney")).timestamp == ZonedDateTime(DateTime(2020, 12, 7, 20, 07), tz"Australia/Sydney")
    # DateTime, local timezone
    @test Banks.Transaction(12.45, DateTime(2020, 12, 7, 20, 07)).timestamp == ZonedDateTime(DateTime(2020, 12, 7, 20, 07), localzone())
    # DateTime, specified timezone
    @test Banks.Transaction(12.45, DateTime(2020, 12, 8, 20, 07), tz"Australia/Sydney").timestamp == ZonedDateTime(DateTime(2020, 12, 8, 20, 07), tz"Australia/Sydney")
    # Default timestamp - possibility of error because now() might differ when used twice so rounding to eliminate small differences
    @test isapprox(Banks.Transaction(12.45, "Burger Wisconsin").timestamp.utc_datetime.instant.periods.value, ZonedDateTime(DateTime(now()), localzone()).utc_datetime.instant.periods.value, atol=100)

    for i in 1:length(times)
        #println(i)
        @test Banks.Transaction(amounts[i], times[i]).amount == amounts[i]
        @test Banks.Transaction(amounts[i], times[i]).timestamp == times[i]
    end
end

@testset "Transaction tests with amount, zoned date time and description" begin
    @test Banks.Transaction(12.45, time1, "The Warehouse Bike Shop").amount == 12.45
    @test Banks.Transaction(12.45, ZonedDateTime(DateTime(2020, 12, 7, 20, 07), localzone()), "The Warehouse Bike Shop").timestamp == ZonedDateTime(DateTime(2020, 12, 7, 20, 07), localzone())
    @test Banks.Transaction(12.45, time1, "The Warehouse Bike Shop").description == "The Warehouse Bike Shop"

    for i in 1:length(txns)
        #println(i)
        @test txns[i].amount == amounts[i]
        @test txns[i].timestamp == times[i]
        @test txns[i].description == descriptions[i]
    end
end

@testset "Transaction tests with just amount" begin
    t = Banks.Transaction(12.45)
    when = now()
    @test t.amount == 12.45
    @test isapprox(t.timestamp.utc_datetime.instant.periods.value, ZonedDateTime(when, localzone()).utc_datetime.instant.periods.value, atol=100)
    @test t.description === nothing

    for i in 1:length(amounts)
        #println(i)
        ti = Banks.Transaction(amounts[i])
        when = now()
        @test ti.amount == amounts[i]
        @test isapprox(ti.timestamp.utc_datetime.instant.periods.value, ZonedDateTime(when, localzone()).utc_datetime.instant.periods.value, atol=100)
        @test ti.description === nothing
    end
end

@testset "+ Transaction tests" begin
    @test Banks.Transaction(amounts[1], times[1], descriptions[1]) + 
        Banks.Transaction(amounts[2], times[2], descriptions[2]) == 
        Banks.Transaction(amounts[1] + amounts[2], max(times[1], times[2]), nothing)
end

#ta = Banks.TransactionAccount(12.45, time1, txns)
#println(ta)

@testset "TransactionAccount tests" begin
    b = Banks.Balance(round(txnssum.amount, digits=2), DateTime(2020, 12, 7, 20, 05))
    @test isapprox(Banks.TransactionAccount("Test", b, txns).balance.amount, txnssum.amount, atol=tolerance)
    @test Banks.TransactionAccount("Test", b, txns).name == "Test"
    @test Banks.TransactionAccount("Test", b, txns).transactions == txns

    # println(Banks.TransactionAccount("Test", b, txns).transactions[1])
    # println(Banks.TransactionAccount("Test", b, txns).transactions[2])

    @test Banks.TransactionAccount("Test", b).balance == b
    @test Banks.TransactionAccount("Test", b).name == "Test"
    @test Banks.TransactionAccount("Test", b).transactions == [Banks.Transaction(round(txnssum.amount, digits=3), b.timestamp, "Initial transaction")]

    # With empty arguments
    @test Banks.TransactionAccount("Test").name == "Test"
    @test Banks.TransactionAccount("Test").transactions == [Banks.Transaction(0., ZonedDateTime(0, tz"UTC"), "Initial transaction")]
end

@testset "Transaction totalling test" begin
    b = Banks.Balance(12.34, DateTime(DateTime(2020, 12, 7, 20, 05)))

    @test Banks.totalamount(Banks.TransactionAccount("Test", b)) == 12.34

    b2 = Banks.Balance(total, DateTime(DateTime(2020, 12, 7, 20, 05)))
    @test isapprox(Banks.totalamount(Banks.TransactionAccount("Test", b2, txns)), total, atol=tolerance)

    # total of transactions and the same negative values should be close to 0.0
    b3 = Banks.Balance(0., DateTime(DateTime(2020, 12, 7, 20, 05)))
    @test isapprox(Banks.totalamount(Banks.TransactionAccount("Test", b3, [txns; txnsneg])), 0., atol=tolerance)

    @debug("sum(t.amount for t in txns): $(sum(t.amount for t in txns))")
    @debug("sum(t.amount for t in txnsneg): $(sum(t.amount for t in txnsneg))")
    @debug("sum(t.amount for t in [txns txnsneg]): $(sum(t.amount for t in [txns; txnsneg]))")
    @debug("Banks.totalamount(txns): $(Banks.totalamount(txns))")
    @debug("Banks.totalamount(txnsneg): $(Banks.totalamount(txnsneg))")
    @debug("Banks.totalamount([txns txnsneg]): $(Banks.totalamount([txns; txnsneg]))")

    @test isapprox(Banks.totalamount(txns), total, atol=tolerance)
    @test isapprox(sum(t.amount for t in txns), total, atol=tolerance)
end

@testset "post! test" begin
    #b = Banks.Balance(12.34, DateTime(DateTime(2020, 12, 7, 20, 05)))
    ta = Banks.TransactionAccount("Test")

    @test Banks.totalamount(ta) == 0.

    t = Banks.Transaction(12.45, ZonedDateTime(DateTime(2020, 12, 7, 20, 07), localzone()), "The Warehouse Bike Shop")
    talike = Banks.Transaction(12.45, ZonedDateTime(DateTime(2020, 12, 7, 20, 07), localzone()), "The Warehouse Bike Shop")
    t2 = Banks.Transaction(123.45, ZonedDateTime(DateTime(2021, 1, 28, 21, 36), localzone()), "Torpedo Eight")

    Banks.post!(ta, t)
    @test Banks.totalamount(ta) == 12.45
    @test ta.balance.amount == Banks.totalamount(ta)

    Banks.post!(ta, t2)
    @test Banks.totalamount(ta) == 12.45 + 123.45
    @test ta.balance.amount == Banks.totalamount(ta)

    #= Same transaction posted again -- this is allowed! Will not throw execption.
    @test_throws ArgumentError Banks.post!(ta, t)
    @test Banks.totalamount(ta) == 12.45 + 123.45
    =#

    Banks.post!(ta, t)
    @test isapprox(Banks.totalamount(ta), 12.45*2 + 123.45, atol=tolerance)
    @test ta.balance.amount == Banks.totalamount(ta)

    # Alike but different
    Banks.post!(ta, talike)
    @test isapprox(Banks.totalamount(ta), 12.45*3 + 123.45, atol=tolerance)
    @test isapprox(Banks.totalamount(ta), 12.45*3 + 123.45, atol=tolerance)

    #@test round(Banks.totalamount(Banks.TransactionAccount("Test", b, [txns txnsneg])), digits=4) == 0.

end